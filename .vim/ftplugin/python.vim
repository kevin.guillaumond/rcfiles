" Add breakpoint
nnoremap <silent> <leader>bb Obreakpoint()<Esc>j
" Set breakpoint so it's hit in nosetest
nnoremap <silent> <leader>bn Ofrom nose.tools import set_trace; set_trace()<Esc>j
" Breakpoint for Python 2
nnoremap <silent> <leader>bv Oimport pdb; pdb.set_trace()<Esc>j

" WIP Do some common lint fixes 
" TODO catch exception on pattern not found
if has('nvim')
  nnoremap <silent> <leader>pl :call PythonLint()<CR>
  function! PythonLint()
    %s/assertEquals/assertEqual/g
    %s/log\.warn(/log.warning(/g
    %s/Exception, e:/Exception as e:/g
  endfunction
endif

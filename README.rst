================
Kevin's dotfiles
================

-----------------
New machine setup
-----------------

.. code-block:: bash

  cd ~
  mkdir .dotfiles
  cd .dotfiles/
  git init
  git remote add origin git@gitlab.com:kevin.guillaumond/rcfiles.git
  git fetch
  git checkout master

Then symlink all the interesting files, like

.. code-block:: bash

  cd
  ln -s .dotfiles/.vimrc

I'm hoping to write a script to automate the symlink creation and update. For
now, I'm maintaining the symlinks manually.

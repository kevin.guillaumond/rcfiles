alias vi="nvim"

alias ll="ls -lhaG"
alias la='ls -A'

# Shortcuts to parent folders
alias   ..='cd ..'
alias  ...='cd ../../' 
alias ....='cd ../../..'

# Ag ignore file
alias ag='ag --path-to-ignore ~/.ignore'

# Local aliases
if [ -f ~/.bash_aliases.local ]; then
  . ~/.bash_aliases.local
fi
